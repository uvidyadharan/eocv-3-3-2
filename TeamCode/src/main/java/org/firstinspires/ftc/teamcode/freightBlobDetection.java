package org.firstinspires.ftc.teamcode;

import org.firstinspires.ftc.robotcore.external.Telemetry;
import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;
import org.opencv.imgproc.Moments;
import org.openftc.easyopencv.OpenCvPipeline;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class freightBlobDetection extends OpenCvPipeline {

    Telemetry telemetry;

    public freightBlobDetection(Telemetry telemetry)
    {
        this.telemetry = telemetry;
    }

    private Mode debugMode = Mode.Lvl3;
    enum Mode {
        Lvl1,
        Lvl2,
        Lvl3,
    }

    public int mode = 0;

    private Mat YCbCr = new Mat();
    private Mat redChannel = new Mat();
    private Mat blueChannel = new Mat();
    private Mat redThresh = new Mat();
    private Mat blueThresh = new Mat();

    private Mat test1 = new Mat(), test2 = new Mat(), test3 = new Mat();

    private Mat HSV = new Mat();
    private Mat HSL = new Mat();

    private Mat hierachy = new Mat();

    private MatOfPoint targetContour;
    private Rect targetRect;

    private Mat YellowThresh = new Mat();
    private List<MatOfPoint> YellowContours = new ArrayList<>();
    private List<MatOfPoint> YellowContoursFilter = new ArrayList<>();

    private int MAX_VAL = 200, blockSize = 5, C = 0;

    public Scalar colorPicker = new Scalar(0,0,0);

    private Scalar lowerYellow = new Scalar(0, 140, 60), upperYellow = new Scalar(256, 180, 110);

    @Override
    public Mat processFrame(Mat input) {

        YellowContours.clear();
        YellowContoursFilter.clear();

        Imgproc.cvtColor(input, YCbCr, Imgproc.COLOR_RGB2YCrCb);

        if(debugMode.ordinal() >= 2)
        {
            Imgproc.cvtColor(input, HSV, Imgproc.COLOR_RGB2HSV);
            Imgproc.cvtColor(input, HSL, Imgproc.COLOR_RGB2HLS);

            Core.extractChannel(YCbCr, redChannel, 1);
            Core.extractChannel(YCbCr, blueChannel, 2);
        }




        Core.inRange(YCbCr, lowerYellow, upperYellow, YellowThresh);




        MatOfInt tree = new MatOfInt();
        ArrayList<MatOfPoint> contours = new ArrayList<>();

        Imgproc.findContours(YellowThresh, contours, tree, Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE);

        ContourTree3 contourTree = new ContourTree3(tree, contours);
        ArrayList<Contour> parents = new ArrayList<>(contourTree.rootContours);

       ContourTree3.filterBySqrtArea(10, parents);


        Imgproc.findContours(YellowThresh, YellowContours, hierachy, Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE);

        //Imgproc.drawContours(input, YellowContours, -1, new Scalar(255, 255, 0));

        telemetry.addData("Hierachy", hierachy);
        telemetry.addData("ctrs: ", parents.size());
        telemetry.update();


        if (debugMode.ordinal() >= 2)
        {
            for(MatOfPoint contour: YellowContours)
            {

                Moments M = Imgproc.moments(contour);
                Imgproc.circle(input, new Point(M.m10/M.m00, M.m01/M.m00), 1, new Scalar(255, 0,0), 1);



            }
        }




        for(Contour contour: parents)
        {
            if (contour.parent != null)
            {
                if(debugMode.ordinal() >= 1)
                {
                    Moments M = Imgproc.moments(contour.self);
                    Imgproc.circle(input, new Point(M.m10/M.m00, M.m01/M.m00), 1, new Scalar(255, 255,0), 1);
                }

                YellowContoursFilter.add(contour.self);
            }else
            {

                if(debugMode.ordinal() >= 1)
                {
                    Moments M = Imgproc.moments(contour.self);
                    Point center = new Point(M.m10/M.m00, M.m01/M.m00);
                    Imgproc.circle(input, center, 2, new Scalar(0, 255,0), 2);
                }

                YellowContoursFilter.add(contour.self);
            }

        }

        if(debugMode.ordinal() >= 1)
        {
            Imgproc.drawContours(input, YellowContoursFilter, -1, new Scalar(0, 255, 255));
        }



        targetContour = Collections.max(YellowContoursFilter, Comparator.comparingDouble(Imgproc::contourArea));

        if(debugMode.ordinal() >= 1)
        {
            targetRect = Imgproc.boundingRect(targetContour);
            Imgproc.rectangle(input, targetRect, new Scalar(255, 0 , 0), 3);
        }

        if(debugMode.ordinal() >= 2)
        {
            Core.bitwise_and(HSL, HSV, test1);
            Core.bitwise_and(HSL, YCbCr, test2);
            Core.bitwise_and(HSV, YCbCr, test3);


            if (mode == 0)
            {
                return input;
            }else if(mode == 1)
            {
                return YCbCr;
            }else if (mode == 2)
            {
                return YellowThresh;
            }else if (mode == 3)
            {
                return HSV;
            }else if (mode == 4)
            {
                return HSL;
            }else if (mode == 5)
            {
                return redThresh;
            }else if (mode == 6)
            {
                return blueThresh;
            }else if (mode == 7)
            {
                return redChannel;
            }else if (mode == 8)
            {
                return blueChannel;
            }else if (mode == 9)
            {
                return test1;
            }else if (mode == 10)
            {
                return test2;
            }else if (mode ==11)
            {
                return test3;
            }

            return input;
        }else
        {
            return input;
        }




    }

    public void setMode(Mode mode){this.debugMode = mode;}

    public Mode getMode(){return this.debugMode;}

    public MatOfPoint getTargetContour(){return targetContour;}

    public Point getTargetCenter() {Moments M = Imgproc.moments(targetContour);
        return new Point(M.m10/M.m00, M.m01/M.m00); }
}
